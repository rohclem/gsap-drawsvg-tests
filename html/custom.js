

////////////////////////
//Below is for drawSVG//
////////////////////////

//for the index
TweenLite.to("#path",3,{drawSVG:"0%"});


//for the circle in second.html
TweenLite.to("#circlepath", 2,{drawSVG:"-100% -100%", ease:Bounce.easeOut})


//for the lines in third.html
TweenLite.from("#linepath", 1.5,{drawSVG:"0", ease:Bounce.easeOut})



//for the lines in fourth.html
TweenLite.from("#mandalapath",2,{drawSVG:"20% 50%", ease:Power0.easeOut})

//for the lines in fourth.html
TweenLite.to("#heartpath",2,{drawSVG:"10%", ease:Bounce.easeOut})

//console.log('test');






/////////////////////////
//Below is for morphSVG//
/////////////////////////
MorphSVGPlugin.convertToPath("polygon");

//for initial morphSVG html
TweenMax.to("#star", 1.5, {morphSVG:{shape:"#arrow",shapeIndex:3}, delay: 0.2, ease:Bounce.easeOut});
//this would work if the SVG is a path, not a polygon because it refers to points = findShapeIndex("#star","#arrow")


//for second morphSVG html
TweenMax.to("#penguin", 1, {morphSVG:{shape:"#phoenix",shapeIndex:1}, delay: 0.1});
//findShapeIndex("#penguin","#phoenix")

//for third morphSVG html
TweenMax.staggerTo("#birdtemplate", 0.2, {cycle:{morphSVG:["#mandalapath1","#mandalapath2","#mandalapath3","#mandalapath4","#mandalapath5","#mandalapath6",
"#mandalapath7","#mandalapath8","#mandalapath9","#mandalapath10","#mandalapath11","#mandalapath12",
"#mandalapath13","#mandalapath14","#mandalapath15","#mandalapath16","#mandalapath17","#mandalapath18",
"#mandalapath19","#mandalapath20","#mandalapath21","#mandalapath22","#mandalapath23","#mandalapath24",
"#mandalapath25","#mandalapath26","#mandalapath27","#mandalapath28","#mandalapath29","#mandalapath30",
"#mandalapath31","#mandalapath32","#mandalapath33","#mandalapath34","#mandalapath35","#mandalapath36",
"#mandalapath37","#mandalapath38","#mandalapath39","#mandalapath40","#mandalapath41","#mandalapath42"]}},0.1);








//fifth morphsvg

MorphSVGPlugin.convertToPath("circle, rect, polygon");

var tl = new TimelineMax({repeat:20, repeatDelay:0.3, yoyo:true});

tl.to("#square", 1, {morphSVG:"#s"})
  .to("#circle", 1, {morphSVG:"#h"})
  .to("#polygon", 1, {morphSVG:"#a"})
  .to("#stars", 1, {morphSVG:"#p"})
  .to("#triangle", 1, {morphSVG:"#e"})
  .to("#rounded", 1, {morphSVG:"#ss"})
  .timeScale(4);


//for fourth morphsvg html

var button = document.getElementById("toggle");

var morph = new TimelineMax({paused:true});
morph.to("#hand", 1, { morphSVG: "#thumbsup", ease:Power2.easeInOut });

button.addEventListener("click", function() {
  if (morph.progress() === 0) { 
    morph.play();
  } else {
    morph.reversed( !morph.reversed() );
  }
});
























