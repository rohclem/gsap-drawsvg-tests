//for the index
TweenLite.from("#s_1", 0.5,{y:"100px", ease:Power3.easeOut})
TweenLite.from("#c_1", 0.5,{y:"100px", delay:0.2,ease:Power2.easeIn})
TweenLite.from("#r_1", 0.5,{y:"100px", delay:0.3,ease:Power3.easeIn})
TweenLite.from("#o1_1", 2,{y:"100px", delay:0.5,ease:Bounce.easeInOut})
TweenLite.from("#l1_1", 0.5,{y:"100px", delay:0.5,ease:Power3.easeIn})
TweenLite.from("#l2_1", 0.5,{y:"100px", delay:0.6,ease:Power3.easeIn})
TweenLite.from("#m_1", 0.5,{y:"100px", delay:0.7,ease:Power3.easeIn})
TweenLite.from("#o2_1", 2,{y:"100px", delay:0.8,ease:Bounce.easeInOut})
TweenLite.from("#t_1", 0.5,{y:"100px", delay:0.9 ,ease:Power3.easeIn})
TweenLite.from("#i_1", 0.5,{y:"100px", delay:0.4,ease:Power3.easeIn})
TweenLite.from("#o3_1", 2,{y:"100px", delay:0.4,ease:Bounce.easeInOut})
TweenLite.from("#n_1", 0.5,{y:"100px", delay:0.4,ease:Power3.easeIn})


//for the index2
var tl2 = new TimelineLite();


var $s_2 = $("#s_2"),
    $c_2 = $("#c_2"),
    $r_2 = $("#r_2"),
    $o1_2 = $("#o1_2"),
    $l1_2 = $("#l1_2"),
    $l2_2 = $("#l2_2"),
    $m_2 = $("#m_2"),
    $o2_2 = $("#o2_2"),
    $t_2 = $("#t_2"),
    $i_2 = $("#i_2"),
    $o3_2 = $("#o3_2"),
    $n_2 = $("#n_2"),
    $pinwheel = $("#pinwheel");



tl2.to($pinwheel, 0.5, {opacity:1, ease:Bounce.easeOut})
   .from($pinwheel, 0.9, {y:"-900px", ease:Bounce.easeOut})
   .to($pinwheel, 0.5, { rotation: "+=360", ease: Bounce.easeOut})
   .to($o1_2, 0.1, {opacity:1, ease:Bounce.easeNone})
   .from($o1_2, 1, {x:"-300px", ease:Elastic.easeOut})
   .to($s_2,.1, {opacity:1, ease:Linear.easeOut})
   .to($c_2,.1, {opacity:1, ease:Linear.easeOut})
   .to($r_2,.1, {opacity:1,  ease:Linear.easeOut})
   .to($o2_2,.3,{opacity:1, ease:Linear.easeNon})
   .from($o2_2, 1, {y:"-400px", ease:Elastic.easeOut})
   .to($o2_2,0.6,{x:"0px",ease:Bounce.easeOut})
   .to($l1_2,1, {opacity:1, delay:1.3, ease:Bounce.easeOut},1)
   .to($l2_2,1, {opacity:1, delay:1.3, ease:Bounce.easeOut},1)
   .to($m_2,1, {opacity:1, delay:1.3, ease:Bounce.easeOut},1)
   .to($o3_2, 1.5, {opacity:1, ease:Linear.easeIn},3)
   .from($o3_2, 2, {scaleX: 4, scaleY:4,opacity:0, ease:Bounce.easeOut},3)
   .to($t_2, 0.8, {opacity:1, delay:0.3,ease:Linear.easeIn},3)
   .to($i_2, 0.8, {opacity:1, delay:0.4,ease:Linear.easeIn},3)
   .to($n_2, 0.8, {opacity:1, delay:0.5,ease:Linear.easeIn},3)
   


//for the index3
TweenLite.to("#s_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.to("#c_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.to("#r_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.from("#o1_3", 0.5,{y:"100px", delay:0.2,ease:Strong.easeInOut})
TweenLite.to("#l1_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.to("#l2_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.to("#m_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.from("#o2_3", 0.5,{y:"100px", delay:0.2,ease:Strong.easeInOut})
TweenLite.to("#t_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.to("#i_3", 0.25,{y:"100px", ease:Power0.easeOut})
TweenLite.from("#o3_3", 0.5,{y:"100px", delay:0.2,ease:Strong.easeInOut})
TweenLite.to("#n_3", 0.25,{y:"100px", ease:Power0.easeIn})


//for the index4

TweenLite.set(["#path_sm_blue_r","#path_sm_blue_o1","#path_sm_blue_s","#path_sm_blue_c","#path_sm_blue_l1","#path_sm_blue_l2"],{opacity:0});
TweenLite.set(["#path_sm_green_o2","#path_sm_green_o3","#path_sm_green_m","#path_sm_green_t","#path_sm_green_i","#path_sm_green_n"],{opacity:0});


TweenLite.from(["#template_sm_blue_r","#template_sm_blue_o1","#template_sm_blue_s","#template_sm_blue_c","#template_sm_blue_l1","#template_sm_blue_l2"], 4,{drawSVG:"-100% -100%", ease:Bounce.easeOut})
TweenLite.to(["#path_sm_blue_r","#path_sm_blue_o1","#path_sm_blue_s","#path_sm_blue_c","#path_sm_blue_l1","#path_sm_blue_l2"], 5, {opacity:1, ease:Linear.easeIn});

TweenLite.from(["#template_sm_green_o2","#template_sm_green_o3","#template_sm_green_m","#template_sm_green_t","#template_sm_green_i","#template_sm_green_n"], 4,{drawSVG:"-100% -100%", ease:Bounce.easeOut})
TweenLite.to(["#path_sm_green_o2","#path_sm_green_o3","#path_sm_green_m","#path_sm_green_t","#path_sm_green_i","#path_sm_green_n"], 5, {opacity:1, ease:Linear.easeIn});



//for the index5

var tl5_1 = new TimelineLite();

TweenLite.set("#r_5", {x:"100px"})
TweenLite.set("#m_5", {y:"300px"})
TweenLite.set("#n_5", {y:"400px"})

tl5_1.to("#s_5", 0.3, {y:"100px", ease:Bounce.easeOut})
     .to("#c_5", 0.3,{y:"100px", ease:Bounce.easeOut})
     .to("#r_5", 0.25,{y:"100px", ease:Bounce.easeOut})
     .to("#r_5", 0.4,{x:"0px", ease:Bounce.easeOut})
     .to("#l1_5", 0.2,{y:"100px", ease:Power4.easeOut})
     .to("#l2_5", 0.2,{y:"100px", ease:Power4.easeOut})
     .to("#m_5", 0.25,{y:"100px", ease:Power3.easeOut})
     .to("#t_5", 0.25,{y:"100px", ease:Power0.easeOut})
     .to("#i_5", 0.25,{y:"100px", ease:Power0.easeOut})
     .to("#n_5", 0.25,{y:"100px", ease:Power0.easeIn})
     .from("#o2_5", 0.50,{y:"100px", ease:Bounce.easeOut})
     .from("#o3_5", 0.50,{y:"-100px", ease:Bounce.easeOut},3)
     .from("#o1_5", 0.30, {y:"100px", ease:Bounce.easeIn},4)


//for index 6
function startCSS(){
  
  tl2.paused(true) // sets paused state to true



};

function playTimeline(){
  //alert("duh");
  tl2.paused(false);
}









